
variable "insance_type" {
  type = string

}
variable "ami" {
  type = string
}

variable "region" {}

variable "key_name" {}
variable "vpc_cidr" {}
variable "public_cidr" {
}
variable "private_cidr" {
}