provider "aws" {
  region = "${var.region}"
  access_key = "AKIA3WDJSYFYEVLMS3MT"
  secret_key = "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"
}
resource "aws_instance" "demo" {
  ami = "${var.ami}"
  instance_type = "${var.insance_type}"
  key_name = "${var.key_name}"
  vpc_security_group_ids = ["${aws_security_group.demo_sg.id}"]
  subnet_id = "${aws_subnet.public_subnet.id}"
  associate_public_ip_address = true
  tags = {
    Name = "terraform_demo"
  }
}
#Vpc creation
resource "aws_vpc" "vpc" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = {
    Name = "my-demo-vpc"
  }
}
#Creating internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "my-demo-igw"
  }
}
#create route table
resource "aws_route_table" "public_route" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }
  tags = {
    Name = "my-demo-public-route"
  }
}
resource "aws_default_route_table" "private_route" {
  default_route_table_id = "${aws_vpc.vpc.default_route_table_id}"

  tags = {
    Name = "My-demo-private-route"
  }
}
#create public subnet
resource "aws_subnet" "public_subnet" {
  cidr_block = "${var.public_cidr}"
  vpc_id = aws_vpc.vpc.id
  map_public_ip_on_launch = true
  tags = {
    Name = "my-demo-public-sub"
  }
}
#create private subnet
resource "aws_subnet" "private_subnet" {
  cidr_block = "${var.private_cidr}"
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "my-demo-private-sub"
  }
}
#Associate public subnet with public route
resource "aws_route_table_association" "public_sub_asso" {
  route_table_id = "${aws_route_table.public_route.id}"
  subnet_id = "${aws_subnet.public_subnet.id}"
  depends_on = ["aws_route_table.public_route","aws_subnet.public_subnet"]
}
#Associate private subnet with public route
resource "aws_route_table_association" "private_sub_asso" {
  count = 2
  route_table_id = "${aws_default_route_table.private_route.id}"
  subnet_id = "${aws_subnet.private_subnet.id}"
  depends_on = ["aws_default_route_table.private_route","aws_subnet.private_subnet"]
}
#security group creation
resource "aws_security_group" "demo_sg" {
  name = "my_demo_sg"
  vpc_id = aws_vpc.vpc.id

}
#Ingree security port 22
resource "aws_security_group_rule" "ssh_inbound_access" {
  from_port = 22
  protocol = "tcp"
  security_group_id = "${aws_security_group.demo_sg.id}"
  to_port = 22
  type = "ingress"
  cidr_blocks = ["0.0.0.0/0"]
}
#Ingree security port 22
resource "aws_security_group_rule" "http_inbound_access" {
  from_port = 80
  protocol = "tcp"
  security_group_id = "${aws_security_group.demo_sg.id}"
  to_port = 80
  type = "ingress"
  cidr_blocks = ["0.0.0.0/0"]
}
#all outbound
resource "aws_security_group_rule" "all_outbound_access" {
  from_port = 0
  protocol = "-1"
  security_group_id = "${aws_security_group.demo_sg.id}"
  to_port = 0
  type = "egress"
  cidr_blocks = ["0.0.0.0/0"]
}
