resource "aws_route53_zone" "demo-zone" {
  name = "jacard.co.in"
  vpc {
    vpc_id = "${var.vpc_id}"
  }
}

resource "aws_route53_record" "demo_record" {
  count = "${length(var.hostname)}"
  name = "${element(var.hostname, count.index)}"
  records = ["${element(var.arecord, count.index)}"]
  zone_id = "${aws_route53_zone.demo-zone.id}"
  type = "A"
  ttl = 300


}