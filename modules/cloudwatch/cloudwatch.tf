resource "aws_cloudwatch_metric_alarm" "cpu_utilization" {
  alarm_name                = "high_cpu_utilization"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/EC2"
  period                    = "120"
  statistic                 = "Average"
  threshold                 = "80"
  alarm_description         = "This metric monitors ec2 cpu utilization"
  insufficient_data_actions = ["${var.sns_topic}"]
  alarm_actions             = ["${var.sns_topic}"]

  dimensions = {
    InstanceID = "${var.instance_id}"
  }
}
resource "aws_cloudwatch_metric_alarm" "instance-health-check" {
  alarm_name          = "instance-health-check"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "StatusCheckFailed"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "1"
  alarm_description   = "This metric monitors ec2 health status"
  alarm_actions       = ["${var.sns_topic}"]

  dimensions = {
    InstanceId = "${var.instance_id}"
  }
}
