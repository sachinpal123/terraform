variable "s3_bucket_name" {
  default = "bucket.com-203"
}
variable "acl" {
  type = string
  default = "private"
}
variable "key" {}
variable "sourcepath" {}