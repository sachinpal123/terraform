resource "aws_s3_bucket" "s3_bucket" {
  bucket = "${var.s3_bucket_name}"
  acl = "${var.acl}"
  force_destroy = true
  lifecycle {
    prevent_destroy = false
  }
  versioning {
    enabled = true
  }
  tags = {
    Name = "${var.s3_bucket_name}"
  }
}
resource "aws_s3_bucket_object" "upload" {
  bucket    = "${aws_s3_bucket.s3_bucket.id}"
  key       = "${var.key}"
  source    = "${var.sourcepath}"

}