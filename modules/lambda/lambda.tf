resource "aws_lambda_function" "demo_lambda" {
  s3_bucket = "${var.s3_bucket}"
  s3_key = "${var.s3_key}"
  function_name = "${var.lambda_function}"
  handler = "main.handler"
  runtime = "python3.7"
  role = "${var.role}"

}