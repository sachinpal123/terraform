variable "instance_type" {}

variable "security_group" {}

variable "subnets" {
  type = "list"
}
variable "ami_id" {}