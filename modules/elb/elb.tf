resource "aws_lb_target_group" "demo_target" {
  health_check {
    interval = 5
    path = "/"
    protocol = "HTTP"
    timeout = 3
    healthy_threshold = 5
    unhealthy_threshold = 2
  }
  name = "my-demo-tg"
  port = 80
  protocol = "HTTP"
  target_type = "instance"
  vpc_id = "${var.vpc_id}"
}
resource "aws_lb_target_group_attachment" "target_group_attachement" {
  target_group_arn = "${aws_lb_target_group.demo_target.arn}"
  target_id        = "${var.instance1_id}"
  port             = 80
}
resource "aws_lb" "my_demo_alb" {
  name = "my-demo-alb"
  internal = false

  security_groups = [
    "${aws_security_group.my_alb_sg.id}"
  ]

  subnets = [
    "${var.subnet1}",
    "${var.subnet2}",
  ]
  tags = {
    Name = "my_demo_lb"
  }
  ip_address_type = "ipv4"
  load_balancer_type = "application"
}
resource "aws_lb_listener" "my_demo_alb_listner" {
  load_balancer_arn = "${aws_lb.my_demo_alb.arn}"
  port = 80
  protocol = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.demo_target.arn}"
  }
}
resource "aws_security_group" "my_alb_sg" {
  name = "my_alb_sg"
  vpc_id = "${var.vpc_id}"
}
resource "aws_security_group_rule" "inbound_ssh" {
  from_port = 22
  protocol = "tcp"
  security_group_id = "${aws_security_group.my_alb_sg.id}"
  to_port = 22
  type = "ingress"
  cidr_blocks = ["0.0.0.0/0"]
}
resource "aws_security_group_rule" "inbound_http" {
  from_port = 80
  protocol = "tcp"
  security_group_id = "${aws_security_group.my_alb_sg.id}"
  to_port = 80
  type = "ingress"
  cidr_blocks = ["0.0.0.0/0"]
}
resource "aws_security_group_rule" "outbound_all" {
  from_port = 0
  protocol = "-1"
  security_group_id = "${aws_security_group.my_alb_sg.id}"
  to_port = 0
  type = "egress"
  cidr_blocks = ["0.0.0.0/0"]
}