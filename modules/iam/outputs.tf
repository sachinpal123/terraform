output "aws_iam_role" {
  value = "${aws_iam_role.lambda_role.arn}"
}

output "eks_cluster_role" {
  value = "${aws_iam_role.eks_cluster.arn}"
}
output "eks_node_role" {
  value = "${aws_iam_role.eks_nodes.arn}"
}