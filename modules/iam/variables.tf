variable "lambda_policy" {
  default = "lambda_policy1"
}
variable "lambda_role" {
  default = "lambda_role1"
}
variable "s3_bucket" {
  default = "bucket.com-202"
}
#-------------cluster----------
variable "eks_cluster" {
  default = "eks_cluster"
}
#--------------node---------------------
variable "eks_node_group" {
  default = "eks_node_group"
}