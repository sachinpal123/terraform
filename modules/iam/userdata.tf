data "template_file" "lambda_policy" {
  template = file("${path.module}/lambda_policy.json")
}
data "template_file" "lambda_assume_role" {
  template = file("${path.module}/lambda_assume_role_policy.json")
}
#---------------EKS---------------------------------------------
data "template_file" "eks_asssume_role" {
  template = file("${path.module}/eks_cluster_assume_role.json")
}

data "template_file" "eks_node_assume_role" {
  template = file("${path.module}/eks_nodes_assume_role.json")
}