resource "aws_iam_policy" "lambda_policy" {
  name = "${var.lambda_policy}"
  policy = "${data.template_file.lambda_policy.rendered}"
}

resource "aws_iam_role" "lambda_role" {
  name = "${var.lambda_role}"
  assume_role_policy = "${data.template_file.lambda_assume_role.rendered}"
}

resource "aws_iam_role_policy_attachment" "iam_policy_attached" {
  role = "${aws_iam_role.lambda_role.name}"
  policy_arn = "${aws_iam_policy.lambda_policy.arn}"
}
#-------------------------------------------------------------------------
#IAM role for EKS
resource "aws_iam_role" "eks_cluster" {
  name = "${var.eks_cluster}"
  assume_role_policy = "${data.template_file.eks_asssume_role.rendered}"
}
resource "aws_iam_role_policy_attachment" "EKSClusterPolicy" {
  policy_arn = "${aws_iam_role.eks_cluster.arn}"
  role       = "${aws_iam_role.eks_cluster.name}"
}
resource "aws_iam_role_policy_attachment" "EKSServicePolicy" {
  policy_arn = "${aws_iam_role.eks_cluster.arn}"
  role       = "${aws_iam_role.eks_cluster.name}"

}

#------------------IAM ROLE FOR NODE-------------------------
resource "aws_iam_role" "eks_nodes" {
  name = "${var.eks_node_group}"
  assume_role_policy = "${data.template_file.eks_node_assume_role.rendered}"
}
resource "aws_iam_role_policy_attachment" "EKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/EKSWorkerNodePolicy"
  role       = aws_iam_role.eks_nodes.name
}

resource "aws_iam_role_policy_attachment" "EKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/EKS_CNI_Policy"
  role       = aws_iam_role.eks_nodes.name
}

resource "aws_iam_role_policy_attachment" "EC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/EC2ContainerRegistryReadOnly"
  role       = aws_iam_role.eks_nodes.name
}
