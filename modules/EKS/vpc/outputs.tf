# Output of VPC

output "vpc-id" {
  description = "The ID of the VPC"
  value       = aws_vpc.eks_vpc.id
}


# Output of IGW

output "igw" {
  value = aws_internet_gateway.eks_igw.*.id
}

# Output of Public Subnet

output "public-subnet-ids" {
  description = "Public Subnets IDS"
  value       = aws_subnet.public_subnets.*.id
}

# Output of EIP For NAT Gateways

output "eip-ngw" {
  value = aws_eip.eip-ngw.*.id
}

# Output Of NAT-Gateways

output "ngw" {
  value = aws_nat_gateway.ngw.*.id
}

# Output Of Private Subnet

output "private-subnet-ids" {
  description = "Private Subnets IDS"
  value       = aws_subnet.private-subnets.*.id
}

# Output Of Public Subnet Associations With Public Route Tables

output "public-association" {
  value = aws_route_table_association.public-association.*.id
}

# Output Of Public Routes

output "aws-route-table-public-routes-id" {
  value = aws_route_table.public-routes.*.id
}

# Output Of Region AZS

output "aws-availability-zones" {
  value = data.aws_availability_zones.availability.names
}

# Output Of Private Route Table ID's

output "aws-route-table-private-routes-id" {
  value = aws_route_table.private-routes.*.id
}
