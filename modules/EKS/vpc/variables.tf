variable "eks_vpc" {
  type        = string
  default     = ""
  description = "Solution name, e.g. `app`"
}
variable "enable-dns-support" {
  default = "true"
}

variable "enable-dns-hostnames" {
  default = "true"
}
variable "eks_igw" {
  description = "Additional tags for the internet gateway"
  default     = "eks_igw"
}
variable "vpc_cidr" {}

variable "public_subnet_cidrs" {
  type = "list"
}
variable "map_public_ip_on_launch" {
}
variable "public-subnet-routes-name" {
  default = "public-routes"
}
variable "public-subnets" {
  default = "public-subnet"
}
variable "public-route-cidr" {
  default = "0.0.0.0/0"
}
#--------------------------------------
variable "private_subnet_cidrs" {
  type = "list"
}
variable "private-subnets" {
  default = "private-subnets"
}
variable "private-route-name" {
  default = "private-routes"
}
# Nat Gate Ways

variable "total-nat-gateway-required" {
  default = "1"
}
# Elastic IP Tags

variable "eip-for-nat-gateway-name" {
  default = "eip-nat-gateway"
}
# Nat Gate Way Tags

variable "nat-gateway-name" {
  default = "nat-gateway"
}
variable "private-route-cidr" {
  default = "0.0.0.0/0"
}
data "aws_availability_zones" "availability" {

}
variable "cluster-name" {
  type = string
}