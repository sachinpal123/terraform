resource "aws_vpc" "eks_vpc" {
  cidr_block = "${var.vpc_cidr}"
  instance_tenancy = "default"
  enable_dns_hostnames = "${var.enable-dns-hostnames}"
  enable_dns_support = "${var.enable-dns-support}"

  tags = {
    Name = "${var.eks_vpc}"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"
  }
}
# Creating Internet Gateway
resource "aws_internet_gateway" "eks_igw" {
  #count = length("${var.public_subnet_cidrs}")
  vpc_id = aws_vpc.eks_vpc.id

  tags = {
    Name = "${var.eks_igw}"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"
  }
}
# Public Subnet
resource "aws_subnet" "public_subnets" {
  count                   = "${length(var.public_subnet_cidrs)}"
  cidr_block              = "${var.public_subnet_cidrs[count.index]}"
  vpc_id                  = "${aws_vpc.eks_vpc.id}"
  map_public_ip_on_launch = "${var.map_public_ip_on_launch}"
  availability_zone       = "${data.aws_availability_zones.availability.names[count.index]}"

  tags = {
    Name = "${var.public-subnets}-${count.index + 1}"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"
    "kubernetes.io/role/elb"  = "1"
  }
}
# Public Routes
resource "aws_route_table" "public-routes" {
  vpc_id = aws_vpc.eks_vpc.id
  route {
    cidr_block = "${var.public-route-cidr}"
    gateway_id = aws_internet_gateway.eks_igw.id
  }
  tags = {
    Name = "${var.public-subnet-routes-name}"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"
  }
}
# Associate/Link Public-Route With Public-Subnets

resource "aws_route_table_association" "public-association" {
  count          = length(var.public_subnet_cidrs)
  route_table_id = aws_route_table.public-routes.id
  subnet_id      = aws_subnet.public_subnets.*.id[count.index]
}
# Creating Private Subnets

resource "aws_subnet" "private-subnets" {
  count = length(var.private_subnet_cidrs)
  availability_zone = data.aws_availability_zones.availability.names[count.index]
  cidr_block = var.private_subnet_cidrs[count.index]
  vpc_id = aws_vpc.eks_vpc.id
  tags = {
    Name = "${var.private-subnets}-${count.index + 1}"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"
  }
}
# Elastic IP For NAT-Gate Way
resource "aws_eip" "eip-ngw" {
  count = var.total-nat-gateway-required
  tags = {
    Name = "${var.eip-for-nat-gateway-name}-${count.index + 1}"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"
  }
}
# Creating NAT Gateways In Public-Subnets

resource "aws_nat_gateway" "ngw" {
  count         = var.total-nat-gateway-required
  allocation_id = aws_eip.eip-ngw.*.id[count.index]
  subnet_id     = aws_subnet.public_subnets.*.id[count.index]
  tags = {
    Name = "${var.nat-gateway-name}-${count.index + 1}"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"

  }
}
# Private Route-Table For Private-Subnets

resource "aws_route_table" "private-routes" {
  count  = length(var.private_subnet_cidrs)
  vpc_id = aws_vpc.eks_vpc.id
  route {
    cidr_block     = var.private-route-cidr
    nat_gateway_id = element(aws_nat_gateway.ngw.*.id,count.index)
  }
  tags = {
    Name = "${var.private-route-name}"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"
  }
}
# Associate/Link Private-Routes With Private-Subnets

resource "aws_route_table_association" "private-association" {
  count          = length(var.private_subnet_cidrs)
  subnet_id      = aws_subnet.private-subnets.*.id[count.index]
  route_table_id = aws_route_table.private-routes.*.id[count.index]
}