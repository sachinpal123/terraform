provider "aws" {
  region = "us-east-1"
  shared_credentials_file = "C:/Users/credentials/.aws/credentials"
  profile = "default"
}
module "eks_vpc" {
  source                     = "../vpc"
  eks_vpc                    = "eks_vpc"
  map_public_ip_on_launch    = "false"
  total-nat-gateway-required = "1"
  vpc_cidr                   = "10.20.0.0/16"
  public_subnet_cidrs        = ["10.20.1.0/24","10.20.2.0/24"]
  private_subnet_cidrs       = ["10.20.4.0/24","10.20.5.0/24"]
  cluster-name               = "eks_master"
}
module "eks_cluster" {
  source = "../eks-cluster-master"
  name = "eks_master"
  vpc_id = "${module.eks_vpc.vpc-id}"
  subnet_ids = "${module.eks_vpc.private-subnet-ids}"
  workers_role_arns = []
  workers_security_group_ids = []
  local_exec_interpreter = "/bin/bash"
}

module "eks_worker" {
  source = "../eks-cluster-worker"
  instance_type = "t3.micro"
  vpc_id = "${module.eks_vpc.vpc-id}"
  subnet_ids = module.eks_vpc.private-subnet-ids
  associate_public_ip_address = false
  health_check_type = "EC2"
  instance_name = "worker_node"
  disk_size = "30"
  desired_size = "2"
  max_size = "3"
  min_size = "2"
  wait_for_capacity_timeout = "10m"
  image_id = "AL2_x86_64"
  cluster_name = "${module.eks_cluster.eks_cluster_name}"
  cluster_endpoint = "${module.eks_cluster.eks_cluster_endpoint}"
  cluster_security_group_id = "${module.eks_cluster.security_group_id}"

}
