variable "vpc_id" {
  type        = string
  description = "VPC ID for the EKS cluster"
}
variable "instance_type" {
  type        = string
  description = "Instance type to launch"
}

variable "subnet_ids" {
  description = "A list of subnet IDs to launch resources in"
  type        = list(string)
}
variable "max_size" {
  type        = number
  description = "The maximum size of the autoscale group"
}

variable "min_size" {
  type        = number
  description = "The minimum size of the autoscale group"
}
variable "associate_public_ip_address" {
  type        = bool
  description = "Associate a public IP address with an instance in a VPC"
  default     = false
}
variable "image_id" {
  type        = string
  description = "EC2 image ID to launch. If not provided, the module will lookup the most recent EKS AMI. See https://docs.aws.amazon.com/eks/latest/userguide/eks-optimized-ami.html for more details on EKS-optimized images"
  default     = ""
}
variable "cluster_name" {
  type        = string
  description = "The name of the EKS cluster"
}
variable "cluster_endpoint" {
  type        = string
  description = "EKS cluster endpoint"
}

variable "cluster_security_group_id" {
  type        = string
  description = "Security Group ID of the EKS cluster"
}
variable "allowed_security_groups" {
  type        = list(string)
  default     = []
  description = "List of Security Group IDs to be allowed to connect to the worker nodes"
}
variable "allowed_cidr_blocks" {
  type        = list(string)
  default     = []
  description = "List of CIDR blocks to be allowed to connect to the worker nodes"
}
variable "use_existing_security_group" {
  type        = string
  default     = 0
}
variable "desired_size" {}
variable "disk_size" {}
variable "instance_name" {}
