data "template_file" "init" {
  template = "${file("${path.module}/userdata.tpl")}"
}

resource "aws_launch_configuration" "launch_config" {
  image_id        = "${var.image_id}"
  instance_type   = "${var.instance_type}"
  security_groups = ["${aws_security_group.asg-sg.id}"]
  key_name        = "jenkins-virgina-new"
  user_data              = "${data.template_file.init.rendered}"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "autoscal_group" {
  launch_configuration = "${aws_launch_configuration.launch_config.name}"
  vpc_zone_identifier = ["${var.subnet1}","${var.subnet2}"]
  target_group_arns = ["${var.target_group_arn}"]
  health_check_type = "ELB"
  health_check_grace_period = 100
  desired_capacity = 2
  force_delete = true


  max_size = 2
  min_size = 1

  tag {
    key = "Name"
    value = "my-instance"
    propagate_at_launch = true
  }
}

resource "aws_security_group" "asg-sg" {
  name   = "my-asg-sg"
  vpc_id = "${var.vpc_id}"
}

resource "aws_security_group_rule" "inbound_ssh" {
  from_port         = 22
  protocol          = "tcp"
  security_group_id = "${aws_security_group.asg-sg.id}"
  to_port           = 22
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "inbound_http" {
  from_port         = 80
  protocol          = "tcp"
  security_group_id = "${aws_security_group.asg-sg.id}"
  to_port           = 80
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "outbound_all" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.asg-sg.id}"
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}
