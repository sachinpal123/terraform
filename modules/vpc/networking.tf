#Vpc creation
resource "aws_vpc" "main" {
  cidr_block = "${var.vpc_cidr}"
  instance_tenancy = "${var.tenancy}"
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = {
    Name = "main"
  }
}
#Creating internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "my-demo-igw"
  }
}
#create route table
resource "aws_route_table" "public_route" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }
  tags = {
    Name = "my-demo-public-route"
  }
}
#private route table
resource "aws_default_route_table" "private_route" {
  default_route_table_id = "${aws_vpc.main.default_route_table_id}"

  tags = {
    Name = "My-demo-private-route"
  }
}
#create public subnet
resource "aws_subnet" "public_subnet" {
  vpc_id = aws_vpc.main.id
  cidr_block = "${var.public_cidr}"

  tags = {
    Name = "public_subnet"
  }
}
#create private subnet
resource "aws_subnet" "private_subnet" {
  cidr_block = "${var.private_cidr}"
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "private-subnet"
  }
}
#Associate public subnet with public route
resource "aws_route_table_association" "public_sub_asso" {
  route_table_id = "${aws_route_table.public_route.id}"
  subnet_id = "${aws_subnet.public_subnet.id}"
  depends_on = ["aws_route_table.public_route","aws_subnet.public_subnet"]
}
#Associate private subnet with public route
resource "aws_route_table_association" "private_sub_asso" {
  route_table_id = "${aws_default_route_table.private_route.id}"
  subnet_id = "${aws_subnet.private_subnet.id}"
  depends_on = ["aws_default_route_table.private_route","aws_subnet.private_subnet"]
}
#security group creation
resource "aws_security_group" "demo_sg" {
  name = "my_demo_sg"
  vpc_id = aws_vpc.main.id

}
#Ingree security port 22
resource "aws_security_group_rule" "ssh_inbound_access" {
  from_port = 22
  protocol = "tcp"
  security_group_id = "${aws_security_group.demo_sg.id}"
  to_port = 22
  type = "ingress"
  cidr_blocks = ["0.0.0.0/0"]
}
#Ingree security port 22
resource "aws_security_group_rule" "http_inbound_access" {
  from_port = 80
  protocol = "tcp"
  security_group_id = "${aws_security_group.demo_sg.id}"
  to_port = 80
  type = "ingress"
  cidr_blocks = ["0.0.0.0/0"]
}
#all outbound
resource "aws_security_group_rule" "all_outbound_access" {
  from_port = 0
  protocol = "-1"
  security_group_id = "${aws_security_group.demo_sg.id}"
  to_port = 0
  type = "egress"
  cidr_blocks = ["0.0.0.0/0"]
}
