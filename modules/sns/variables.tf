variable "sns_topic_name" {
  type = string
  default = "my_demo_sns"
  description = "sns topic name"
}

variable "alarms_email" {}