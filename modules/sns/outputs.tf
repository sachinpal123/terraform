output "sns_topic_arn" {
  value = join("",aws_sns_topic.my_sns.*.arn)
}