provider "aws" {
  region = "us-east-1"
  shared_credentials_file = "C:/Users/credentials/.aws/credentials"
  profile = "default"
}
terraform {
  backend "s3" {
    shared_credentials_file = "C:/Users/credentials/.aws/credentials"
    profile = "default"
    bucket = "terraform-state-tf-file"
    key = "dev/terraform.tfstate"
    region = "us-east-1"
  }
}
module "vpc" {
  source          = "../vpc"
  vpc_cidr        = "10.0.0.0/16"
  public_cidrs    = ["10.0.1.0/24", "10.0.2.0/24"]
  private_cidrs   = ["10.0.3.0/24", "10.0.4.0/24"]
}

module "ec2" {
  source          = "../ec2"
  instance_type   = "t2.micro"
  ami_id          = "ami-0e16018da2c7dedd3"
  security_group  = "${module.vpc.security_group}"
  subnets         = "${module.vpc.public_subnets}"
}
module "autoscale" {
  source            = "../autoscalling"
  image_id          = "ami-0e16018da2c7dedd3"
  instance_type     = "t2.micro"
  vpc_id            = "${module.vpc.vpc_id}"
  subnet1           = "${module.vpc.subnet1}"
  subnet2           = "${module.vpc.subnet2}"
  target_group_arn  = "${module.elb.alb_target_group_arn}"
}
/*
module "rds" {
  source = "../rds"
  db_instance = "db.t2.micro"
  rds_subnet1 = "${module.vpc.private_subnet1}"
  rds_subnet2 = "${module.vpc.private_subnet2}"
  vpc_id      = "${module.vpc.vpc_id}"
}
*/

/*module "iam" {
  source = "../iam"
}
module "s3" {
  source = "../s3"
  s3_bucket_name = "bucket.com-203"
  key       = "lambda_policy.json"
  sourcepath = "${path.module}/lambda_policy.json"
}*/
module "elb" {
  source       = "../elb"
  vpc_id       = "${module.vpc.vpc_id}"
  instance1_id = "${module.ec2.instance1_id}"
  subnet1      = "${module.vpc.subnet1}"
  subnet2      = "${module.vpc.subnet2}"
}
/*
module "sns" {
  source = "../sns"
  alarms_email = "sachinpal0064@gmail.com"
}

module "cloudwatch" {
  source = "../cloudwatch"
  sns_topic = "${module.sns.sns_topic_arn}"
  instance_id = "${module.ec2.instance1_id}"
}
*/
/*module "my_lambda" {
  source          = "../lambda"
  s3_bucket       = "${module.s3.s3_bucket_name}"
  s3_key          = "lambda_policy.json"
  role            = "${module.iam.aws_iam_role}"
}*/

module "route53" {
  source = "../route53"
  hostname = ["jacard.co.in"]
  arecord  = ["10.0.1.11", "10.0.1.12"]
  vpc_id = "${module.vpc.vpc_id}"
}